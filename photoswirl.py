"""Photo swirl laadt net als met Katana een foto in stukjes."""
from tkinter import Canvas, Tk, NW

from PIL import Image, ImageTk
import argparse

BLOCKSIZE = 15


class Tile:
    def __init__(self, tile, x, y):
        self.x = x
        self.y = y
        self.tile = tile
        self.shown = False


class FullImage:
    def __init__(self, image_name, size=(0, 0)):
        self.root = Tk()
        self.im = Image.open(image_name)
        image_x = self.im.width
        image_y = self.im.height
        if size[0] + size[1] > 0:
            image_x, image_y = size
        self.canvas = Canvas(self.root, width=image_x, height=image_y)
        self.canvas.pack()
        self.tiles = []
        self.max_y = int(image_x / BLOCKSIZE)
        self.max_x = int(image_y / BLOCKSIZE)

        for x in range(self.max_x):
            for y in range(self.max_y):
                crop_rectangle = (x * BLOCKSIZE, y * BLOCKSIZE, BLOCKSIZE, BLOCKSIZE)
                tile = Tile(self.im.crop(crop_rectangle), x * BLOCKSIZE, y * BLOCKSIZE)
                self.tiles.append(tile)

        photoimg = ImageTk.PhotoImage(self.im)  # show the entire image
        self.canvas.create_image(0, 0, anchor=NW, image=photoimg)
        self.root.mainloop()


def parse_command():
    parser = argparse.ArgumentParser(description='Show a picture swirl in')
    parser.add_argument('pics', nargs='+', help='List of pictures to show')
    parser.add_argument('--width', '-W', action='store_const', default=1280, const='width',
                        help='Width of the (scaled) image. Defaults to 1280')
    parser.add_argument('--height', '-H', action='store_const', default=720, const='height',
                        help='Height of the (scaled) image. Defaults to 720')
    parser.add_argument('--blocksize', '-b', action='store_const', default=15, const='blocksize',
                        help='Size of the blocks of the image')

    # Here we get a lot of possible flags

    return parser.parse_args()


def show_image(image):
    """Show the image directly"""
    FullImage(image, )


def main():
    arguments = parse_command()
    print(f"We openen in het formaat {arguments.width}*{arguments.height} de bestanden:")
    for picture in arguments.pics:
        print(picture)
        FullImage(picture, (arguments.width, arguments.height))


if __name__ == "__main__":
    main()
