# PhotoSwirl
PhotoSwirl is een klein python programmatje dat een foto laadt en in stukjes binnen vliegt. Daartoe wordt de foto in blokjes tegels geknipt van 15x15 pixels
(tenzij we op de commando regel wat andere formaten aangeven) en in groepjes van 5 per seconden ze random laten verschijnen.

Dat verschijnen gebeurt aan de rechter kant waarna ze op hun plek vliegen.

De tegels zijn een beetje versprongen en worden zeer vaag vooraf getoond.

## Strategie om te tonen
We delen de image in in voorgedefinieerde blokjes en stoppen die in een lineare array. De blokjes zijn van een class waarin
we de eindcoordinaten van het blokje zetten, of het zichtbaar is en plaatsen de instanties in een array. Daarui pakken we
dan random een blokje vanaf de linker kant.

De array moet steeds van boven naar onder worden opgebouwd.
